#include <ctime>
#include <cmath>
#include <iostream>

#include <boost/multi_array.hpp>
#include <boost/format.hpp>

using namespace std;
using namespace boost;

typedef double FltPnt;

int vnorm(FltPnt vx, FltPnt vy, FltPnt vz, FltPnt &norm);

int solidAngle(FltPnt  x,  FltPnt y,  FltPnt z,
               FltPnt  x0, FltPnt y0, FltPnt z0,
               FltPnt  x1, FltPnt y1, FltPnt z1,
               FltPnt  x2, FltPnt y2, FltPnt z2,
               FltPnt &omega);

int WeFunction(FltPnt  x,  FltPnt y,  FltPnt z,
               FltPnt  x0, FltPnt y0, FltPnt z0,
               FltPnt  x1, FltPnt y1, FltPnt z1,
               FltPnt &We);

int WfFunction(FltPnt  x,  FltPnt  y,  FltPnt z,
               FltPnt  x0, FltPnt  y0, FltPnt z0,
               FltPnt  x1, FltPnt  y1, FltPnt z1,
               FltPnt  x2, FltPnt  y2, FltPnt z2,
               FltPnt  nx, FltPnt  ny, FltPnt nz,
               FltPnt  fx, FltPnt  fy, FltPnt fz,
               FltPnt &Wf);

int GradWfFunction(FltPnt  x,    FltPnt  y,    FltPnt  z,
                   FltPnt  x0,   FltPnt  y0,   FltPnt  z0,
                   FltPnt  x1,   FltPnt  y1,   FltPnt  z1,
                   FltPnt  x2,   FltPnt  y2,   FltPnt  z2,
                   FltPnt  nx,   FltPnt  ny,   FltPnt  nz,
                   FltPnt  fx,   FltPnt  fy,   FltPnt  fz,
                   FltPnt &GWfx, FltPnt &GWfy, FltPnt &GWfz);

int vectorPotential(FltPnt x,  FltPnt y,  FltPnt z,
                    FltPnt mx, FltPnt my, FltPnt mz,
                    boost::multi_array<FltPnt, 2> const &vcl,
                    boost::multi_array<FltPnt, 2> const &n,
                    boost::multi_array<FltPnt, 2> const &f,
                    boost::multi_array<int,    2> const &til,
                    FltPnt &Ax, FltPnt &Ay, FltPnt &Az);

int magneticFluxDensity(FltPnt x,  FltPnt y,  FltPnt z,
                        FltPnt mx, FltPnt my, FltPnt mz,
                        boost::multi_array<FltPnt, 2> const &vcl,
                        boost::multi_array<FltPnt, 2> const &n,
                        boost::multi_array<FltPnt, 2> const &f,
                        boost::multi_array<int,    2> const &til,
                        FltPnt &Bx, FltPnt &By, FltPnt &Bz);

int main(int argc, char **argv)
{
  cout << "sizeof(long double): " << sizeof(long double) << endl;
  cout << "sizeof(double):      " << sizeof(double) << endl;
  cout << "sizeof(FltPnt):      " << sizeof(FltPnt) << endl;
  FltPnt dvcl[24][3] = {
    { 0.0527245,  0.0225962,  0.0000000}, 
    { 0.0527245,  0.0000000, -0.0225962}, 
    { 0.0527245, -0.0225962,  0.0000000}, 
    { 0.0527245,  0.0000000,  0.0225962}, 
    {-0.0527245,  0.0225962,  0.0000000}, 
    {-0.0527245,  0.0000000, -0.0225962}, 
    {-0.0527245, -0.0225962,  0.0000000}, 
    {-0.0527245,  0.0000000,  0.0225962}, 
    { 0.0000000,  0.0527245,  0.0225962}, 
    {-0.0225962,  0.0527245,  0.0000000}, 
    { 0.0000000,  0.0527245, -0.0225962}, 
    { 0.0225962,  0.0527245,  0.0000000}, 
    { 0.0000000, -0.0527245,  0.0225962}, 
    {-0.0225962, -0.0527245,  0.0000000}, 
    { 0.0000000, -0.0527245, -0.0225962}, 
    { 0.0225962, -0.0527245,  0.0000000}, 
    { 0.0000000,  0.0225962,  0.0527245}, 
    { 0.0225962,  0.0000000,  0.0527245}, 
    { 0.0000000, -0.0225962,  0.0527245}, 
    {-0.0225962,  0.0000000,  0.0527245}, 
    { 0.0000000,  0.0225962, -0.0527245}, 
    { 0.0225962,  0.0000000, -0.0527245}, 
    { 0.0000000, -0.0225962, -0.0527245}, 
    {-0.0225962,  0.0000000, -0.0527245} };
  int dtil[44][3] = {
    { 0,  2,  1}, { 0,  3,  2}, { 4,  5,  6}, { 6,  7,  4}, {11,  9,  8}, 
    {11, 10,  9}, {14, 15, 12}, {14, 12, 13}, {18, 17, 16}, {18, 16, 19}, 
    {20, 21, 22}, {20, 22, 23}, {19,  7,  6}, {18, 13, 12}, {19,  6, 13}, 
    {18, 19, 13}, { 8,  9,  4}, {16,  7, 19}, { 8,  4,  7}, { 8,  7, 16}, 
    {10, 20, 23}, { 9,  5,  4}, {10, 23,  5}, {10,  5,  9}, { 5, 23, 22}, 
    { 5, 22, 14}, { 5, 14,  6}, { 6, 14, 13}, {20, 10, 11}, {20, 11, 21}, 
    {21, 11,  0}, {21,  0,  1}, { 1,  2, 15}, { 1, 15, 14}, { 1, 14, 21}, 
    {21, 14, 22}, { 2,  3, 17}, { 2, 17, 18}, { 2, 18, 15}, {15, 18, 12}, 
    { 0, 11,  8}, { 0,  8, 16}, { 0, 16,  3}, { 3, 16, 17}
  };
  FltPnt dn[44][3] = {
    { 1.00000,  0.00000,  0.00000}, { 1.00000,  0.00000,  0.00000}, 
    {-1.00000,  0.00000,  0.00000}, {-1.00000,  0.00000,  0.00000}, 
    { 0.00000,  1.00000,  0.00000}, { 0.00000,  1.00000,  0.00000}, 
    { 0.00000, -1.00000,  0.00000}, { 0.00000, -1.00000,  0.00000}, 
    { 0.00000,  0.00000,  1.00000}, { 0.00000,  0.00000,  1.00000}, 
    { 0.00000,  0.00000, -1.00000}, { 0.00000,  0.00000, -1.00000}, 
    {-0.57735, -0.57735,  0.57735}, {-0.57735, -0.57735,  0.57735}, 
    {-0.57735, -0.57735,  0.57735}, {-0.57735, -0.57735,  0.57735},
    {-0.57735,  0.57735,  0.57735}, {-0.57735,  0.57735,  0.57735}, 
    {-0.57735,  0.57735,  0.57735}, {-0.57735,  0.57735,  0.57735},
    {-0.57735,  0.57735, -0.57735}, {-0.57735,  0.57735, -0.57735}, 
    {-0.57735,  0.57735, -0.57735}, {-0.57735,  0.57735, -0.57735}, 
    {-0.57735, -0.57735, -0.57735}, {-0.57735, -0.57735, -0.57735}, 
    {-0.57735, -0.57735, -0.57735}, {-0.57735, -0.57735, -0.57735}, 
    { 0.57735,  0.57735, -0.57735}, { 0.57735,  0.57735, -0.57735}, 
    { 0.57735,  0.57735, -0.57735}, { 0.57735,  0.57735, -0.57735}, 
    { 0.57735, -0.57735, -0.57735}, { 0.57735, -0.57735, -0.57735}, 
    { 0.57735, -0.57735, -0.57735}, { 0.57735, -0.57735, -0.57735}, 
    { 0.57735, -0.57735,  0.57735}, { 0.57735, -0.57735,  0.57735}, 
    { 0.57735, -0.57735,  0.57735}, { 0.57735, -0.57735,  0.57735}, 
    { 0.57735,  0.57735,  0.57735}, { 0.57735,  0.57735,  0.57735},
    { 0.57735,  0.57735,  0.57735}, { 0.57735,  0.57735,  0.57735} };
  FltPnt df[44][3] = {
    { 0.05272450,  0.00000000, -0.00753207}, { 0.05272450,  0.00000000,  0.00753207},
    {-0.05272450,  0.00000000, -0.00753207}, {-0.05272450,  0.00000000,  0.00753207}, 
    { 0.00000000,  0.05272450,  0.00753207}, { 0.00000000,  0.05272450, -0.00753207}, 
    { 0.00753207, -0.05272450,  0.00000000}, {-0.00753207, -0.05272450,  0.00000000}, 
    { 0.00753207,  0.00000000,  0.05272450}, {-0.00753207,  0.00000000,  0.05272450}, 
    { 0.00753207,  0.00000000, -0.05272450}, {-0.00753207,  0.00000000, -0.05272450}, 
    {-0.04268170, -0.00753207,  0.02510690}, {-0.00753207, -0.04268170,  0.02510690}, 
    {-0.03263900, -0.02510690,  0.01757480}, {-0.01506410, -0.02510690,  0.03514970},
    {-0.02510690,  0.04268170,  0.00753207}, {-0.02510690,  0.00753207,  0.04268170}, 
    {-0.03514970,  0.02510690,  0.01506410}, {-0.01757480,  0.02510690,  0.03263900},
    {-0.00753207,  0.02510690, -0.04268170}, {-0.04268170,  0.02510690, -0.00753207}, 
    {-0.02510690,  0.01757480, -0.03263900}, {-0.02510690,  0.03514970, -0.01506410}, 
    {-0.02510690, -0.00753207, -0.04268170}, {-0.01757480, -0.02510690, -0.03263900}, 
    {-0.03514970, -0.02510690, -0.01506410}, {-0.02510690, -0.04268170, -0.00753207}, 
    { 0.00753207,  0.04268170, -0.02510690}, { 0.01506410,  0.02510690, -0.03514970},
    { 0.03263900,  0.02510690, -0.01757480}, { 0.04268170,  0.00753207, -0.02510690}, 
    { 0.04268170, -0.02510690, -0.00753207}, { 0.02510690, -0.03514970, -0.01506410}, 
    { 0.02510690, -0.01757480, -0.03263900}, { 0.00753207, -0.02510690, -0.04268170}, 
    { 0.04268170, -0.00753207,  0.02510690}, { 0.02510690, -0.01506410,  0.03514970}, 
    { 0.02510690, -0.03263900,  0.01757480}, { 0.00753207, -0.04268170,  0.02510690},
    { 0.02510690,  0.04268170,  0.00753207}, { 0.01757480,  0.03263900,  0.02510690}, 
    { 0.03514970,  0.01506410,  0.02510690}, { 0.02510690,  0.00753207,  0.04268170} };

  boost::multi_array<FltPnt, 2> vcl(boost::extents[24][3]);
  for (int i = 0; i < 24; ++i)
    for (int j = 0; j < 3; ++j) 
      vcl[i][j] = dvcl[i][j];
  
  boost::multi_array<int, 2>    til(boost::extents[44][3]);
  for (int i = 0; i < 44; ++i)
    for (int j = 0; j < 3; ++j)
      til[i][j] = dtil[i][j];

  boost::multi_array<FltPnt, 2> f(boost::extents[44][3]);
  for (int i = 0; i < 44; ++i)
    for (int j = 0; j < 3; ++j)
      f[i][j] = df[i][j];

  boost::multi_array<FltPnt, 2> n(boost::extents[44][3]);
  for (int i = 0; i < 44; ++i)
    for (int j = 0; j < 3; ++j)
      n[i][j] = dn[i][j];

  FltPnt Ax = 0.0, Ay = 0.0, Az = 0.0;
  FltPnt mx = 0.0, my = 0.0, mz = 1.0;
  FltPnt x = 0.0,  y = -0.025,  z = 0.0;
  FltPnt GWfx = 0.0, GWfy = 0.0, GWfz = 0.0;
  FltPnt Bx = 0.0, By = 0.0, Bz = 0.0;

  /////////////////////////////////////////////////////////////////////////////
  // Examining vector potential calculation.                                 //
  /////////////////////////////////////////////////////////////////////////////
  vectorPotential(x,  y,  z,
                  mx, my, mz,
                  vcl, n, f, til,
                  Ax, Ay, Az);

  cout << "Ax: " << Ax << endl;
  cout << "Ay: " << Ay << endl;
  cout << "Az: " << Az << endl;

  /////////////////////////////////////////////////////////////////////////////
  // Examining magnetic flux density calculation.                            //
  /////////////////////////////////////////////////////////////////////////////
  magneticFluxDensity(x,  y,  z,
                      mx, my, mz,
                      vcl, n, f, til,
                      Bx, By, Bz);

  cout << "Bx: " << Bx << endl;
  cout << "By: " << By << endl;
  cout << "Bz: " << Bz << endl;

  /////////////////////////////////////////////////////////////////////////////
  // Examining solidAngle calculation.                                       //
  /////////////////////////////////////////////////////////////////////////////
  FltPnt omega;
  solidAngle(x,                 y,                 z,
             vcl[til[0][0]][0], vcl[til[0][0]][1], vcl[til[0][0]][2],
             vcl[til[0][1]][0], vcl[til[0][1]][1], vcl[til[0][1]][2],
             vcl[til[0][2]][0], vcl[til[0][2]][1], vcl[til[0][2]][2],
             omega);
  cout << "omega = " << omega << endl;

  /////////////////////////////////////////////////////////////////////////////
  // Examining Wf function calculation.                                      //
  /////////////////////////////////////////////////////////////////////////////
  FltPnt wf;
  WfFunction(x,                 y,                 z,
             vcl[til[0][0]][0], vcl[til[0][0]][1], vcl[til[0][0]][2],
             vcl[til[0][1]][0], vcl[til[0][1]][1], vcl[til[0][1]][2],
             vcl[til[0][2]][0], vcl[til[0][2]][1], vcl[til[0][2]][2],
             n[0][0],           n[0][1],           n[0][2],
             f[0][0],           f[0][1],           f[0][2],
             wf);
  cout << "wf = " << wf << endl;

  /////////////////////////////////////////////////////////////////////////////
  // Examining the GWf calculation.                                          //
  /////////////////////////////////////////////////////////////////////////////
  GradWfFunction(x,                 y,                 z,
                 vcl[til[0][0]][0], vcl[til[0][0]][1], vcl[til[0][0]][2],
                 vcl[til[0][1]][0], vcl[til[0][1]][1], vcl[til[0][1]][2],
                 vcl[til[0][2]][0], vcl[til[0][2]][1], vcl[til[0][2]][2],
                 n[0][0],           n[0][1],           n[0][2],
                 f[0][0],           f[0][1],           f[0][2],
                 GWfx,              GWfy,              GWfz);
  cout << "GWf = (" << GWfx << ", " << GWfy << ", " << GWfz << ")" << endl;


  /////////////////////////////////////////////////////////////////////////////
  // Examining We function calculation.                                      //
  /////////////////////////////////////////////////////////////////////////////
  FltPnt we;
  WeFunction(x,                 y,                 z,
             vcl[til[0][0]][0], vcl[til[0][0]][1], vcl[til[0][0]][2],
             vcl[til[0][1]][0], vcl[til[0][1]][1], vcl[til[0][1]][2],
             we);
  cout << "we = " << we << endl;
}

int vnorm(FltPnt vx, FltPnt vy, FltPnt vz, FltPnt &norm) 
{
  FltPnt eps = 1E-8;
  norm = sqrt(vx*vx + vy*vy + vz*vz + eps*eps);
  return -1;
}

int solidAngle(FltPnt  x,  FltPnt y,  FltPnt z,
               FltPnt  x0, FltPnt y0, FltPnt z0,
               FltPnt  x1, FltPnt y1, FltPnt z1,
               FltPnt  x2, FltPnt y2, FltPnt z2,
               FltPnt &omega)
{
  FltPnt norm1, norm2, norm3;
  vnorm(x0-x, y0-y, z0-z, norm1);
  vnorm(x1-x, y1-y, z1-z, norm2);
  vnorm(x2-x, y2-y, z2-z, norm3);

  FltPnt a = 
      norm1*norm2*norm3
    + norm3*((x0-x)*(x1-x) + (y0-y)*(y1-y) + (z0-z)*(z1-z))
    + norm2*((x0-x)*(x2-x) + (y0-y)*(y2-y) + (z0-z)*(z2-z))
    + norm1*((x1-x)*(x2-x) + (y1-y)*(y2-y) + (z1-z)*(z2-z));

  FltPnt b = 
      (z - z0)*(x2*(y1 - y) + x1*(y - y2) + x*(y2 - y1)) 
    + (y - y0)*(x2*(z - z1) + x1*(z2 - z) + x*(z1 - z2)) 
    + (x - x0)*(y2*(z1 - z) + y1*(z - z2) + y*(z2 - z1));

  omega = 2.0 * atan2(b, a);

  return -1;
}

int WeFunction(FltPnt  x,  FltPnt y,  FltPnt z,
               FltPnt  x0, FltPnt y0, FltPnt z0,
               FltPnt  x1, FltPnt y1, FltPnt z1,
               FltPnt &we)
{
  FltPnt norm1, norm2, norm3;
  vnorm(x1-x,  y1-y,  z1-z,  norm1); 
  vnorm(x0-x,  y0-y,  z0-z,  norm2);
  vnorm(x1-x0, y1-y0, z1-z0, norm3);

  FltPnt a = norm1 + norm2 + norm3;
  FltPnt b = norm1 + norm2 - norm3;
  
  we = log(a/b);

  return -1;
}

int WfFunction(FltPnt  x,  FltPnt y,  FltPnt z,
               FltPnt  x0, FltPnt y0, FltPnt z0,
               FltPnt  x1, FltPnt y1, FltPnt z1,
               FltPnt  x2, FltPnt y2, FltPnt z2,
               FltPnt  nx, FltPnt ny, FltPnt nz,
               FltPnt  fx, FltPnt fy, FltPnt fz,
               FltPnt &Wf)
{
  FltPnt Omega;
  solidAngle(x,  y,  z,
             x0, y0, z0,
             x1, y1, z1, 
             x2, y2, z2,
             Omega);

  FltPnt u0x = x1 - x0, u0y = y1 - y0, u0z = z1 - z0;
  FltPnt unorm0 = sqrt(u0x*u0x + u0y*u0y + u0z*u0z);
  u0x /= unorm0; u0y /= unorm0; u0z /= unorm0;

  FltPnt u1x = x2 - x1, u1y = y2 - y1, u1z = z2 - z1;
  FltPnt unorm1 = sqrt(u1x*u1x + u1y*u1y + u1z*u1z);
  u1x /= unorm1; u1y /= unorm1; u1z /= unorm1;

  FltPnt u2x = x0 - x2, u2y = y0 - y2, u2z = z0 - z2;
  FltPnt unorm2 = sqrt(u2x*u2x + u2y*u2y + u2z*u2z);
  u2x /= unorm2; u2y /= unorm2; u2z /= unorm2;

  FltPnt We1, We2, We3;
  WeFunction(x,  y,  z,
             x0, y0, z0,
             x1, y1, z1,
             We1);

  WeFunction(x,  y,  z,
             x2, y2, z2,
             x1, y1, z1,
             We2);

  WeFunction(x,  y,  z,
             x0, y0, z0,
             x2, y2, z2,
             We3);

  Wf =
    We1*(u0z*(ny*x - ny*x0 - nx*y + nx*y0) + u0y*(-(nz*x) + nz*x0 + nx*z - nx*z0) + 
         u0x*(nz*y - nz*y0 - ny*z + ny*z0)) + 
    We2*(u1z*(ny*x - ny*x1 - nx*y + nx*y1) + u1y*(-(nz*x) + nz*x1 + nx*z - nx*z1) + 
         u1x*(nz*y - nz*y1 - ny*z + ny*z1)) + 
    We3*(u2z*(ny*x - ny*x2 - nx*y + nx*y2) + u2y*(-(nz*x) + nz*x2 + nx*z - nx*z2) + 
         u2x*(nz*y - nz*y2 - ny*z + ny*z2)) 
    -(nx*(fx - x) + ny*(fy - y) + nz*(fz - z))*Omega;

  return -1;
}

int GradWfFunction(FltPnt   x,   FltPnt   y,   FltPnt   z,
                   FltPnt  x0,   FltPnt  y0,   FltPnt  z0,
                   FltPnt  x1,   FltPnt  y1,   FltPnt  z1,
                   FltPnt  x2,   FltPnt  y2,   FltPnt  z2,
                   FltPnt  nx,   FltPnt  ny,   FltPnt  nz,
                   FltPnt  fx,   FltPnt  fy,   FltPnt  fz,
                   FltPnt &GWfx, FltPnt &GWfy, FltPnt &GWfz) 
{
  FltPnt Omega;
  solidAngle(x,  y,  z,
             x0, y0, z0,
             x1, y1, z1, 
             x2, y2, z2,
             Omega);

  FltPnt u0x = x1 - x0, u0y = y1 - y0, u0z = z1 - z0;
  FltPnt unorm0 = sqrt(u0x*u0x + u0y*u0y + u0z*u0z);
  u0x /= unorm0; u0y /= unorm0; u0z /= unorm0;

  FltPnt u1x = x2 - x1, u1y = y2 - y1, u1z = z2 - z1;
  FltPnt unorm1 = sqrt(u1x*u1x + u1y*u1y + u1z*u1z);
  u1x /= unorm1; u1y /= unorm1; u1z /= unorm1;

  FltPnt u2x = x0 - x2, u2y = y0 - y2, u2z = z0 - z2;
  FltPnt unorm2 = sqrt(u2x*u2x + u2y*u2y + u2z*u2z);
  u2x /= unorm2; u2y /= unorm2; u2z /= unorm2;

  FltPnt We1, We2, We3;
  WeFunction(x,  y,  z,
             x0, y0, z0,
             x1, y1, z1,
             We1);

  WeFunction(x,  y,  z,
             x2, y2, z2,
             x1, y1, z1,
             We2);

  WeFunction(x,  y,  z,
             x0, y0, z0,
             x2, y2, z2,
             We3);

  GWfx =   (-(nz*u0y) + ny*u0z)*We1 
         + (-(nz*u1y) + ny*u1z)*We2 
         + (-(nz*u2y) + ny*u2z)*We3 + nx*Omega;

  GWfy =     (nz*u0x  - nx*u0z)*We1 
         +   (nz*u1x  - nx*u1z)*We2 
         +   (nz*u2x  - nx*u2z)*We3 + ny*Omega;

  GWfz =   (-(ny*u0x) + nx*u0y)*We1 
         + (-(ny*u1x) + nx*u1y)*We2 
         + (-(ny*u2x) + nx*u2y)*We3 + nz*Omega;

  return -1;
}

int vectorPotential(FltPnt  x, FltPnt  y, FltPnt  z,
                    FltPnt mx, FltPnt my, FltPnt mz,
                    boost::multi_array<FltPnt, 2> const &vcl,
                    boost::multi_array<FltPnt, 2> const &n,
                    boost::multi_array<FltPnt, 2> const &f,
                    boost::multi_array<int,    2> const &til,
                    FltPnt &Ax, FltPnt &Ay, FltPnt &Az)
{
  Ax = 0.0; Ay = 0.0; Az = 0.0;
  for (int i = 0; i < til.shape()[0]; ++i) {
    FltPnt nx = n[i][0],            ny = n[i][1],            nz = n[i][2];
    FltPnt fx = f[i][0],            fy = f[i][1],            fz = f[i][2];
    FltPnt x0 = vcl[til[i][0]][0],  y0 = vcl[til[i][0]][1],  z0 = vcl[til[i][0]][2];
    FltPnt x1 = vcl[til[i][1]][0],  y1 = vcl[til[i][1]][1],  z1 = vcl[til[i][1]][2];
    FltPnt x2 = vcl[til[i][2]][0],  y2 = vcl[til[i][2]][1],  z2 = vcl[til[i][2]][2];

    FltPnt cx = my*nz - mz*ny,  cy = mz*nx - mx*nz,  cz = mx*ny - my*nx;

    FltPnt Wf;
    WfFunction(x,  y,  z, 
               x0, y0, z0,
               x1, y1, z1,
               x2, y2, z2,
               nx, ny, nz,
               fx, fy, fz,
               Wf);
    
    Ax += Wf * cx;  Ay += Wf * cy;  Az += Wf * cz;
  }

  return -1;
}

int magneticFluxDensity(FltPnt x,  FltPnt y,  FltPnt z,
                        FltPnt mx, FltPnt my, FltPnt mz,
                        boost::multi_array<FltPnt, 2> const &vcl,
                        boost::multi_array<FltPnt, 2> const &n,
                        boost::multi_array<FltPnt, 2> const &f,
                        boost::multi_array<int,    2> const &til,
                        FltPnt &Bx, FltPnt &By, FltPnt &Bz)
{
  Bx = 0.0; By = 0.0; Bz = 0.0;
  for (int i = 0; i < til.shape()[0]; ++i) {
    FltPnt nx = n[i][0],            ny = n[i][1],            nz = n[i][2];
    FltPnt fx = f[i][0],            fy = f[i][1],            fz = f[i][2];
    FltPnt x0 = vcl[til[i][0]][0],  y0 = vcl[til[i][0]][1],  z0 = vcl[til[i][0]][2];
    FltPnt x1 = vcl[til[i][1]][0],  y1 = vcl[til[i][1]][1],  z1 = vcl[til[i][1]][2];
    FltPnt x2 = vcl[til[i][2]][0],  y2 = vcl[til[i][2]][1],  z2 = vcl[til[i][2]][2];

    FltPnt GWfx = 0.0, GWfy = 0.0, GWfz = 0.0;

    GradWfFunction(x,    y,    z, 
                   x0,   y0,   z0,
                   x1,   y1,   z1,
                   x2,   y2,   z2,
                   nx,   ny,   nz,
                   fx,   fy,   fz,
                   GWfx, GWfy, GWfz);

    FltPnt cx =   GWfy*my*nx  + GWfz*mz*nx - GWfy*mx*ny - GWfz*mx*nz;
    FltPnt cy = -(GWfx*my*nx) + GWfx*mx*ny + GWfz*mz*ny - GWfz*my*nz;
    FltPnt cz = -(GWfx*mz*nx) - GWfy*mz*ny + GWfx*mx*nz + GWfy*my*nz;

    cout << "Iteration " << i << ", {" << cx << ", " << cy << ", " << cz << "}" << endl;

    Bx = Bx + cx;  By = By + cy;  Bz = Bz + cz;
  }

  return -1;
}
