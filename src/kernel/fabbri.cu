#include<cmath>
#include<iostream>

#include "fabbri.h"

#define THREADS_PER_BLOCK 64

/**
 * Calculate the vector norm of a three-component Cartesian vector.
 * @param [in]  vx    the x component of the vector
 * @param [in]  vy    the y component of the vector
 * @param [in]  vz    the z component of the vector
 * @param [in]  eps   the epsilon value (defined in Fabbri, 2008)
 * @param [out] norm  the norm of the vector
 */
__device__ 
void vnorm(double vx, double vy, double vz, double eps, double *norm) 
{
  *norm = sqrt(vx*vx + vy*vy + vz*vz + eps*eps);
}

/**
 * Calculate the cenroid of a triangle defined by Cartesian coordinate vertices
 * r0 = (x0, y0, z0), r1 = (x1, y1, z2) and r2 = (x2, y2, z2).
 * @param [in]  x0 the x component of the zeroth triangle point
 * @param [in]  y0 the y component of the zeroth triangle point
 * @param [in]  z0 the z component of the zeroth triangle point
 * @param [in]  x1 the x component of the first triangle point
 * @param [in]  y1 the y component of the first triangle point
 * @param [in]  z1 the z component of the first triangle point
 * @param [in]  x2 the x component of the second triangle point
 * @param [in]  y2 the y component of the second triangle point
 * @param [in]  z2 the z component of the second triangle point
 * @param [out] fx the x component of the centroid   
 * @param [out] fy the y component of the centroid   
 * @param [out] fz the z component of the centroid   
 */ 
__device__
void centroid(double  x0, double  y0, double  z0,
              double  x1, double  y1, double  z1,
              double  x2, double  y2, double  z2,
              double *fx, double *fy, double *fz)
{
    *fx = (x0 + x1 + x2)/3.0;
    *fy = (y0 + y1 + y2)/3.0;
    *fz = (z0 + z1 + z2)/3.0;
}

/**
 * Calculate the normal vector of a triangle defined by Cartesian coordinate
 * vertices r0 = (x0, y0, z0), r1 = (x1, y1, z1) and r2 = (x2, y2, z2). NOTE:
 * the vertex sequence (r0, r1, r2) *must* have positive winding.
 * @param [in]  x0 the x component of the zeroth triangle point
 * @param [in]  y0 the y component of the zeroth triangle point
 * @param [in]  z0 the z component of the zeroth triangle point
 * @param [in]  x1 the x component of the first triangle point
 * @param [in]  y1 the y component of the first triangle point
 * @param [in]  z1 the z component of the first triangle point
 * @param [in]  x2 the x component of the second triangle point
 * @param [in]  y2 the y component of the second triangle point
 * @param [in]  z2 the z component of the second triangle point
 * @param [out] nx the x component of the normal   
 * @param [out] ny the y component of the normal   
 * @param [out] nz the z component of the normal   
 */
__device__
void normalVector(double  x0, double  y0, double  z0,
                  double  x1, double  y1, double  z1,
                  double  x2, double  y2, double  z2,
                  double *nx, double *ny, double *nz)
{ double len;
  *nx = -y1*z0 + y2*z0 + y0*z1 - y2*z1 - y0*z2 + y1*z2;
  *ny =  x1*z0 - x2*z0 - x0*z1 + x2*z1 + x0*z2 - x1*z2;
  *nz = -x1*y0 + x2*y0 + x0*y1 - x2*y1 - x0*y2 + x1*y2;
  
  len = sqrt((*nx)*(*nx) + (*ny)*(*ny) + (*nz)*(*nz));

  *nx = *nx / len;
  *ny = *ny / len;
  *nz = *nz / len;

} 

/**
 * Calculate the solid angle at a Cartesian test point (x, y, z) with respect to 
 * the triangle defined by Cartesian coordinate vertices r0 = (x0, y0, z0), 
 * r1 = (x1, y1, z1) and r2 = (x2, y2, z2). 
 * @param [in]   x    the x component of the test point
 * @param [in]   y    the y component of the test point
 * @param [in]   z    the z component of the test point
 * @param [in]  x0    the x component of the zeroth triangle point
 * @param [in]  y0    the y component of the zeroth triangle point
 * @param [in]  z0    the z component of the zeroth triangle point
 * @param [in]  x1    the x component of the first triangle point
 * @param [in]  y1    the y component of the first triangle point
 * @param [in]  z1    the z component of the first triangle point
 * @param [in]  x2    the x component of the second triangle point
 * @param [in]  y2    the y component of the second triangle point
 * @param [in]  z2    the z component of the second triangle point
 * @param [in]  eps   the epsilon value (defined in Fabbri, 2008)
 * @param [out] omega the solid angle
 */
__device__
void solidAngle(double  x,  double y,  double z,
                double  x0, double y0, double z0,
                double  x1, double y1, double z1,
                double  x2, double y2, double z2,
                double  eps,
                double *omega)
{
  double norm1, norm2, norm3;
  vnorm(x0-x, y0-y, z0-z, eps, &norm1);
  vnorm(x1-x, y1-y, z1-z, eps, &norm2);
  vnorm(x2-x, y2-y, z2-z, eps, &norm3);

  double a = 
      norm1*norm2*norm3
    + norm3*((x0-x)*(x1-x) + (y0-y)*(y1-y) + (z0-z)*(z1-z))
    + norm2*((x0-x)*(x2-x) + (y0-y)*(y2-y) + (z0-z)*(z2-z))
    + norm1*((x1-x)*(x2-x) + (y1-y)*(y2-y) + (z1-z)*(z2-z));

  double b = 
      (z - z0)*(x2*(y1 - y) + x1*(y - y2) + x*(y2 - y1)) 
    + (y - y0)*(x2*(z - z1) + x1*(z2 - z) + x*(z1 - z2)) 
    + (x - x0)*(y2*(z1 - z) + y1*(z - z2) + y*(z2 - z1));

  *omega = 2.0 * atan2(b, a);
}

/**
 * Calculate the We function (defined in Fabbri, 2008) at a Cartesian test 
 * point (x, y, z) due to a line defined by Cartesian vertices r0 = (x0, y0, z0)
 * and r1 = (x0, y0, z0).
 * @param [in]   x  the x component of the test point
 * @param [in]   y  the y component of the test point
 * @param [in]   z  the z component of the test point
 * @param [in]  x0  the x component of the zeroth line point
 * @param [in]  y0  the y component of the zeroth line point
 * @param [in]  z0  the z component of the zeroth line point
 * @param [in]  x1  the x component of the first line point
 * @param [in]  y1  the y component of the first line point
 * @param [in]  z1  the z component of the first line point
 * @param [in]  eps the epsilon value (defined in Fabbri, 2008)
 * @param [out] we  the solid angle
 */
__device__
void WeFunction(double x,  double y,  double z,
                double x0, double y0, double z0,
                double x1, double y1, double z1,
                double eps,
                double *we)
{
  double norm1, norm2, norm3;
  vnorm(x1-x,  y1-y,  z1-z,  eps, &norm1); 
  vnorm(x0-x,  y0-y,  z0-z,  eps, &norm2);
  vnorm(x1-x0, y1-y0, z1-z0, eps, &norm3);

  double a = norm1 + norm2 + norm3;
  double b = norm1 + norm2 - norm3;
  
  *we = log(a/b);
}

/**
 * Calculate the Wf function (defined in Fabbri, 2008) at a Cartesian test 
 * point (x, y, z) w.r.t. to the triangle defined by Cartesian vertices
 * r0 = (x0, y0, z0), r1 = (x1, y1, z1), r2 = (x2, y2, z2). NOTE: the vertex
 * sequence (r0, r1, r2) *must* have positive winding as it is used to calculate
 * a normal vector for the triangle.
 * @param [in]   x  the x component of the test point
 * @param [in]   y  the y component of the test point
 * @param [in]   z  the z component of the test point
 * @param [in]  x0  the x component of the zeroth triangle point
 * @param [in]  y0  the y component of the zeroth triangle point
 * @param [in]  z0  the z component of the zeroth triangle point
 * @param [in]  x1  the x component of the first triangle point
 * @param [in]  y1  the y component of the first triangle point
 * @param [in]  z1  the z component of the first triangle point
 * @param [in]  eps the epsilon value (defined in Fabbri, 2008)
 * @param [out] we  the value of We w.r.t. the line
 */
__device__
void WfFunction(double x,  double y,  double z,
                double x0, double y0, double z0,
                double x1, double y1, double z1,
                double x2, double y2, double z2,
                double eps,
                double *Wf)
{
  double Omega;
  solidAngle(x,  y,  z,
             x0, y0, z0,
             x1, y1, z1, 
             x2, y2, z2,
             eps, 
             &Omega);

  double fx = 0.0, fy = 0.0, fz = 0.0;
  double nx = 0.0, ny = 0.0, nz = 0.0;
  centroid(x0, y0, z0,
           x1, y1, z1,
           x2, y2, z2,
           &fx, &fy, &fz);
  normalVector(x0, y0, z0,
               x1, y1, z1,
               x2, y2, z2,
               &nx, &ny, &nz);

  double u0x = x1 - x0, u0y = y1 - y0, u0z = z1 - z0;
  double unorm0 = sqrt(u0x*u0x + u0y*u0y + u0z*u0z);
  u0x /= unorm0; u0y /= unorm0; u0z /= unorm0;

  double u1x = x2 - x1, u1y = y2 - y1, u1z = z2 - z1;
  double unorm1 = sqrt(u1x*u1x + u1y*u1y + u1z*u1z);
  u1x /= unorm1; u1y /= unorm1; u1z /= unorm1;

  double u2x = x0 - x2, u2y = y0 - y2, u2z = z0 - z2;
  double unorm2 = sqrt(u2x*u2x + u2y*u2y + u2z*u2z);
  u2x /= unorm2; u2y /= unorm2; u2z /= unorm2;

  double We1, We2, We3;
  WeFunction(x,  y,  z,
             x0, y0, z0,
             x1, y1, z1,
             eps,
             &We1);

  WeFunction(x,  y,  z,
             x2, y2, z2,
             x1, y1, z1,
             eps,
             &We2);

  WeFunction(x,  y,  z,
             x0, y0, z0,
             x2, y2, z2,
             eps,
             &We3);

  *Wf =
    We1*(u0z*(ny*x - ny*x0 - nx*y + nx*y0) + u0y*(-(nz*x) + nz*x0 + nx*z - nx*z0) + 
         u0x*(nz*y - nz*y0 - ny*z + ny*z0)) + 
    We2*(u1z*(ny*x - ny*x1 - nx*y + nx*y1) + u1y*(-(nz*x) + nz*x1 + nx*z - nx*z1) + 
         u1x*(nz*y - nz*y1 - ny*z + ny*z1)) + 
    We3*(u2z*(ny*x - ny*x2 - nx*y + nx*y2) + u2y*(-(nz*x) + nz*x2 + nx*z - nx*z2) + 
         u2x*(nz*y - nz*y2 - ny*z + ny*z2)) 
    -(nx*(fx - x) + ny*(fy - y) + nz*(fz - z))*Omega;
}

/**
 * Calculate the gradient of the Wf function (defined in Fabbri, 2008) at a 
 * Cartesian test point (x, y, z) w.r.t. to the triangle defined by 
 * Cartesian vertices r0 = (x0, y0, z0), r1 = (x1, y1, z1), r2 = (x2, y2, z2). 
 * NOTE: the vertex sequence (r0, r1, r2) *must* have a positive winding as it
 * is used to calculate a normal vector for the triangle.
 * @param [in]   x  the x component of the test point
 * @param [in]   y  the y component of the test point
 * @param [in]   z  the z component of the test point
 * @param [in]  x0  the x component of the zeroth triangle point
 * @param [in]  y0  the y component of the zeroth triangle point
 * @param [in]  z0  the z component of the zeroth triangle point
 * @param [in]  x1  the x component of the first triangle point
 * @param [in]  y1  the y component of the first triangle point
 * @param [in]  z1  the z component of the first triangle point
 * @param [in]  x2  the x component of the second triangle point
 * @param [in]  y2  the y component of the second triangle point
 * @param [in]  z2  the z component of the second triangle point
 * @param [in]  eps the epsilon value (defined in Fabbri, 2008)
 * @param [out] Gfx Cartesian x component of grad(We) w.r.t. the line
 * @param [out] Gfy Cartesian y component of grad(We) w.r.t. the line
 * @param [out] Gfz Cartesian z component of grad(We) w.r.t. the line
 */
__device__
void GradWfFunction(double x,  double  y,  double  z,
                    double x0, double  y0, double  z0,
                    double x1, double  y1, double  z1,
                    double x2, double  y2, double  z2,
                    double eps,
                    double *GWfx, double *GWfy, double *GWfz) 
{
  double Omega;
  solidAngle(x,  y,  z,
             x0, y0, z0,
             x1, y1, z1, 
             x2, y2, z2,
             eps,
             &Omega);

  double fx = 0.0, fy = 0.0, fz = 0.0;
  double nx = 0.0, ny = 0.0, nz = 0.0;
  centroid(x0, y0, z0,
           x1, y1, z1,
           x2, y2, z2,
           &fx, &fy, &fz);
  normalVector(x0, y0, z0,
               x1, y1, z1,
               x2, y2, z2,
               &nx, &ny, &nz);

  double u0x = x1 - x0, u0y = y1 - y0, u0z = z1 - z0;
  double unorm0 = sqrt(u0x*u0x + u0y*u0y + u0z*u0z);
  u0x /= unorm0; u0y /= unorm0; u0z /= unorm0;

  double u1x = x2 - x1, u1y = y2 - y1, u1z = z2 - z1;
  double unorm1 = sqrt(u1x*u1x + u1y*u1y + u1z*u1z);
  u1x /= unorm1; u1y /= unorm1; u1z /= unorm1;

  double u2x = x0 - x2, u2y = y0 - y2, u2z = z0 - z2;
  double unorm2 = sqrt(u2x*u2x + u2y*u2y + u2z*u2z);
  u2x /= unorm2; u2y /= unorm2; u2z /= unorm2;

  double We1, We2, We3;
  WeFunction(x,  y,  z,
             x0, y0, z0,
             x1, y1, z1,
             eps,
             &We1);

  WeFunction(x,  y,  z,
             x2, y2, z2,
             x1, y1, z1,
             eps,
             &We2);

  WeFunction(x,  y,  z,
             x0, y0, z0,
             x2, y2, z2,
             eps,
             &We3);

  *GWfx =  (-(nz*u0y) + ny*u0z)*We1 
         + (-(nz*u1y) + ny*u1z)*We2 
         + (-(nz*u2y) + ny*u2z)*We3 + nx*Omega;

  *GWfy =  (nz*u0x  - nx*u0z)*We1 
         + (nz*u1x  - nx*u1z)*We2 
         + (nz*u2x  - nx*u2z)*We3 + ny*Omega;

  *GWfz =  (-(ny*u0x) + nx*u0y)*We1 
         + (-(ny*u1x) + nx*u1y)*We2 
         + (-(ny*u2x) + nx*u2y)*We3 + nz*Omega;
}

/**
 * Kernel to calculate the vector potential at a series of points due to the
 * geometry defined by a list of triangles.
 * @tpts  [in]  test points at which the vector potetial is calculated (the 
 *              size of this array is 3 by `ntpts`).
 * @ntpts [in]  the number of test points.
 * @tri   [in]  the vertices of the triangles which define the geometry for 
 *              for which we attempt to calculate the vector potential (the 
 *              size of this array is 9 by `ntri` with three Cartesian 
 *              coordinates for each vertex of three components - it is assumed
 *              that the order of the coordinates has positive winding).
 * @ntri  [in]  the number of triangles.
 * @mx    [in]  the x-component of the magnetisation.
 * @my    [in]  the y-component of the magnetisation.
 * @mz    [in]  the z-component of the magnetisation.
 * @eps   [in]  the eps error used for the normal (see Fabbri, 2008).
 * @mu0   [in]  the permeability of free space (S.I 4*pi*10^-7 [N A^{-2}])
 * @A     [out] the vector potential at each of the point defined in `tpts`.
 */
__global__
void VectorPotentialKernel(double *tpts, int ntpts,
                           double *tri,  int ntri,
                           double mx, double my, double mz,
                           double eps, double mu0,
                           double *A)
{
  // Test (and A) point id.
  int pid = blockDim.x*blockIdx.x + threadIdx.x; 
  // Surface triangle id.
  int tid = 0;                

  double x  = 0.0, y  = 0.0, z  = 0.0;
  double nx = 0.0, ny = 0.0, nz = 0.0;
  double x0 = 0.0, y0 = 0.0, z0 = 0.0;
  double x1 = 0.0, y1 = 0.0, z1 = 0.0;
  double x2 = 0.0, y2 = 0.0, z2 = 0.0;
    
  double crossX = 0.0, crossY = 0.0, crossZ = 0.0;

  double Wf = 0.0;

  double Ax = 0, Ay = 0, Az = 0;
  
  double CONST = (mu0)/(4.0*M_PI);

  if (pid < ntpts) {

    x = tpts[pid*3 + 0];
    y = tpts[pid*3 + 1];
    z = tpts[pid*3 + 2];

    for (tid = 0; tid < ntri; ++tid) {

      x0 = tri[tid*9+0]; y0 = tri[tid*9+1]; z0 = tri[tid*9+2];
      x1 = tri[tid*9+3]; y1 = tri[tid*9+4]; z1 = tri[tid*9+5];
      x2 = tri[tid*9+6]; y2 = tri[tid*9+7]; z2 = tri[tid*9+8];

      normalVector(x0, y0, z0, x1, y1, z1, x2, y2, z2, &nx, &ny, &nz);

      WfFunction(x,  y,  z, 
                 x0, y0, z0,
                 x1, y1, z1,
                 x2, y2, z2,
                 eps,
                 &Wf);

      crossX = Wf*(my*nz - mz*ny); 
      crossY = Wf*(mz*nx - mx*nz); 
      crossZ = Wf*(mx*ny - my*nx);
            
      Ax += crossX;  Ay += crossY;  Az += crossZ;

    }

    A[pid*3 + 0] = CONST*Ax;
    A[pid*3 + 1] = CONST*Ay;
    A[pid*3 + 2] = CONST*Az;

  }

}

__global__
void MagneticFluxDensityKernel(double *tpts, int ntpts,
                               double *tri,  int ntri,
                               double mx, double my, double mz,
                               double eps, double mu0,
                               double *B)
{
  // Test (and B) point id.
  int pid = blockDim.x*blockIdx.x + threadIdx.x;
  // Surface triangle id.
  int tid = 0;

  double x  = 0.0, y  = 0.0, z  = 0.0;
  double nx = 0.0, ny = 0.0, nz = 0.0;
  double x0 = 0.0, y0 = 0.0, z0 = 0.0;
  double x1 = 0.0, y1 = 0.0, z1 = 0.0;
  double x2 = 0.0, y2 = 0.0, z2 = 0.0;
    
  double crossX = 0.0, crossY = 0.0, crossZ = 0.0;

  double GWfx = 0.0, GWfy = 0.0, GWfz = 0.0;

  double Bx = 0, By = 0, Bz = 0;

  double CONST = (-1.0*mu0)/(4.0*M_PI);

  if (pid < ntpts) {
    x = tpts[pid*3 + 0];
    y = tpts[pid*3 + 1];
    z = tpts[pid*3 + 2];

    for (tid = 0; tid < ntri; ++tid) {

      x0 = tri[tid*9+0]; y0 = tri[tid*9+1]; z0 = tri[tid*9+2];
      x1 = tri[tid*9+3]; y1 = tri[tid*9+4]; z1 = tri[tid*9+5];
      x2 = tri[tid*9+6]; y2 = tri[tid*9+7]; z2 = tri[tid*9+8];

      normalVector(x0, y0, z0, x1, y1, z1, x2, y2, z2, &nx, &ny, &nz);

      GradWfFunction( x,     y,     z,
                      x0,    y0,    z0,
                      x1,    y1,    z1,
                      x2,    y2,    z2,
                      eps,
                     &GWfx, &GWfy, &GWfz);

      crossX =   GWfy*my*nx  + GWfz*mz*nx - GWfy*mx*ny - GWfz*mx*nz;
      crossY = -(GWfx*my*nx) + GWfx*mx*ny + GWfz*mz*ny - GWfz*my*nz;
      crossZ = -(GWfx*mz*nx) - GWfy*mz*ny + GWfx*mx*nz + GWfy*my*nz;

      Bx += crossX; By += crossY; Bz += crossZ;

    }

    B[pid*3 + 0] = CONST*Bx;
    B[pid*3 + 1] = CONST*By;
    B[pid*3 + 2] = CONST*Bz;
    
  }

}

__global__
void DemagnetizingFieldKernel(double *tpts, int ntpts,
                              double *tri,  int ntri,
                              double mx, double my, double mz,
                              double eps, double mu0,
                              double *H)
{
  // Test (and B) point id.
  int pid = blockDim.x*blockIdx.x + threadIdx.x;
  // Surface triangle id.
  int tid = 0;

  double x  = 0.0, y  = 0.0, z  = 0.0;
  double nx = 0.0, ny = 0.0, nz = 0.0;
  double fx = 0.0, fy = 0.0, fz = 0.0;
  double ox = 0.0, oy = 0.0, oz = 0.0;
  double x0 = 0.0, y0 = 0.0, z0 = 0.0;
  double x1 = 0.0, y1 = 0.0, z1 = 0.0;
  double x2 = 0.0, y2 = 0.0, z2 = 0.0;

  double len = 0.0;

  bool isInside = true;
    
  double crossX = 0.0, crossY = 0.0, crossZ = 0.0;

  double GWfx = 0.0, GWfy = 0.0, GWfz = 0.0;

  double Bx = 0, By = 0, Bz = 0;

  double CONST = (-1.0)/(4.0*M_PI);

  if (pid < ntpts) {
    x = tpts[pid*3 + 0];
    y = tpts[pid*3 + 1];
    z = tpts[pid*3 + 2];

    for (tid = 0; tid < ntri; ++tid) {

      x0 = tri[tid*9+0]; y0 = tri[tid*9+1]; z0 = tri[tid*9+2];
      x1 = tri[tid*9+3]; y1 = tri[tid*9+4]; z1 = tri[tid*9+5];
      x2 = tri[tid*9+6]; y2 = tri[tid*9+7]; z2 = tri[tid*9+8];

      centroid(x0, y0, z0, x1, y1, z1, x2, y2, z2, &fx, &fy, &fz);
      normalVector(x0, y0, z0, x1, y1, z1, x2, y2, z2, &nx, &ny, &nz);

      GradWfFunction( x,     y,     z,
                      x0,    y0,    z0,
                      x1,    y1,    z1,
                      x2,    y2,    z2,
                      eps,
                     &GWfx, &GWfy, &GWfz);

      crossX =   GWfy*my*nx  + GWfz*mz*nx - GWfy*mx*ny - GWfz*mx*nz;
      crossY = -(GWfx*my*nx) + GWfx*mx*ny + GWfz*mz*ny - GWfz*my*nz;
      crossZ = -(GWfx*mz*nx) - GWfy*mz*ny + GWfx*mx*nz + GWfy*my*nz;

      // Accumulate the magnetic flux density for the point x,y,z.
      Bx += crossX; By += crossY; Bz += crossZ;
      
      // Calculate the o (outside test) vector.
      ox = fx - x; oy = fy - y; oz = fz - z;
      len = sqrt(ox*ox + oy*oy + oz*oz);
      ox = ox/len; oy = oy/len; oz = oz/len;

      // Check for 'outside-ness' (i.e. if dot product of o and n vectors is 
      // greater than zero.
      isInside = isInside && ((ox*nx + oy*ny + oz*nz) > 0.0);

    }

    if (isInside) {
      H[pid*3 + 0] = CONST*Bx - mx;
      H[pid*3 + 1] = CONST*By - my;
      H[pid*3 + 2] = CONST*Bz - mz;
    } else {
      H[pid*3 + 0] = 0.0;
      H[pid*3 + 1] = 0.0;
      H[pid*3 + 2] = 0.0;
    }
    
  }

}

///////////////////////////////////////////////////////////////////////////////
// Launcher code for kernels.                                                //
///////////////////////////////////////////////////////////////////////////////

using namespace boost;
using namespace std;

void launch_VectorPotentialKernel(
    multi_array<double, 2> const &tpts,
    multi_array<double, 2> const &vcl,
    multi_array<int,    2> const &til,
    multi_array<double, 2> const &m, 
    double                       eps,
    double                       mu0,
    multi_array<double, 2>       &A)
{
  double x0 = 0.0, y0 = 0.0, z0 = 0.0;
  double x1 = 0.0, y1 = 0.0, z1 = 0.0;
  double x2 = 0.0, y2 = 0.0, z2 = 0.0;

  double mx = m[0][0], my = m[0][1], mz = m[0][2];

  unsigned int ntpts = tpts.shape()[0];
  unsigned int ntri = til.shape()[0];

  double *d_tpts;
  double *d_tri;
  double *d_A;

  int nblocks = 0;  

  // Resize the output array.
  cout << "Resizing vector potentials array" << endl;
  A.resize(extents[ntpts][3]);
  cout << "... OK!" << endl;

  // Construct the triangle array.
  cout << "Setting up triangles array." << endl;
  multi_array<double, 2> tri(extents[ntri][9]);
  for (int i = 0; i < ntri; ++i) {
    x0 = vcl[til[i][0]][0]; y0 = vcl[til[i][0]][1]; z0 = vcl[til[i][0]][2];
    x1 = vcl[til[i][1]][0]; y1 = vcl[til[i][1]][1]; z1 = vcl[til[i][1]][2];
    x2 = vcl[til[i][2]][0]; y2 = vcl[til[i][2]][1]; z2 = vcl[til[i][2]][2];
    tri[i][0] = x0; tri[i][1] = y0; tri[i][2] = z0;
    tri[i][3] = x1; tri[i][4] = y1; tri[i][5] = z1;
    tri[i][6] = x2; tri[i][7] = y2; tri[i][8] = z2;
  }
  cout << "... OK!" << endl;

  // Create storage on the device.
  cudaMalloc(&d_tpts, 3*ntpts*sizeof(double));
  cudaMalloc(&d_tri,  9*ntri*sizeof(double));
  cudaMalloc(&d_A,    3*ntpts*sizeof(double));

  // Copy memory over to the device.
  cudaMemcpy(d_tpts, tpts.data(), 3*ntpts*sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(d_tri,  tri.data(),  9*ntri*sizeof(double),  cudaMemcpyHostToDevice);
  cudaMemcpy(d_A,    A.data(),    3*ntpts*sizeof(double), cudaMemcpyHostToDevice);

  // Execute the kernel.
  nblocks = ceil((double)ntpts / (double)THREADS_PER_BLOCK);
  VectorPotentialKernel<<<nblocks, THREADS_PER_BLOCK>>>(
      d_tpts, ntpts, d_tri,  ntri, mx, my, mz, eps, mu0, d_A);
  cudaDeviceSynchronize();

  // Copy back the memory.
  cudaMemcpy(A.data(), d_A, 3*ntpts*sizeof(double), cudaMemcpyDeviceToHost);

  // Free device memory.
  cudaFree(d_tpts);
  cudaFree(d_tri);
  cudaFree(d_A);
}

void launch_MagneticFluxDensityKernel(
    multi_array<double, 2> const &tpts,
    multi_array<double, 2> const &vcl,
    multi_array<int,    2> const &til,
    multi_array<double, 2> const &m, 
    double                       eps,
    double                       mu0,
    multi_array<double, 2>       &A)
{
  double x0 = 0.0, y0 = 0.0, z0 = 0.0;
  double x1 = 0.0, y1 = 0.0, z1 = 0.0;
  double x2 = 0.0, y2 = 0.0, z2 = 0.0;

  double mx = m[0][0], my = m[0][1], mz = m[0][2];

  unsigned int ntpts = tpts.shape()[0];
  unsigned int ntri = til.shape()[0];

  double *d_tpts;
  double *d_tri;
  double *d_A;

  int nblocks = 0;  

  // Resize the output array.
  cout << "Resizing vector potentials array" << endl;
  A.resize(extents[ntpts][3]);
  cout << "... OK!" << endl;

  // Construct the triangle array.
  cout << "Setting up triangles array." << endl;
  multi_array<double, 2> tri(extents[ntri][9]);
  for (int i = 0; i < ntri; ++i) {
    x0 = vcl[til[i][0]][0]; y0 = vcl[til[i][0]][1]; z0 = vcl[til[i][0]][2];
    x1 = vcl[til[i][1]][0]; y1 = vcl[til[i][1]][1]; z1 = vcl[til[i][1]][2];
    x2 = vcl[til[i][2]][0]; y2 = vcl[til[i][2]][1]; z2 = vcl[til[i][2]][2];
    tri[i][0] = x0; tri[i][1] = y0; tri[i][2] = z0;
    tri[i][3] = x1; tri[i][4] = y1; tri[i][5] = z1;
    tri[i][6] = x2; tri[i][7] = y2; tri[i][8] = z2;
  }
  cout << "... OK!" << endl;

  // Create storage on the device.
  cudaMalloc(&d_tpts, 3*ntpts*sizeof(double));
  cudaMalloc(&d_tri,  9*ntri*sizeof(double));
  cudaMalloc(&d_A,    3*ntpts*sizeof(double));

  // Copy memory over to the device.
  cudaMemcpy(d_tpts, tpts.data(), 3*ntpts*sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(d_tri,  tri.data(),  9*ntri*sizeof(double),  cudaMemcpyHostToDevice);
  cudaMemcpy(d_A,    A.data(),    3*ntpts*sizeof(double), cudaMemcpyHostToDevice);

  // Execute the kernel.
  nblocks = ceil((double)ntpts / (double)THREADS_PER_BLOCK);
  MagneticFluxDensityKernel<<<nblocks, THREADS_PER_BLOCK>>>(
      d_tpts, ntpts, d_tri,  ntri, mx, my, mz, eps, mu0, d_A);
  cudaDeviceSynchronize();

  // Copy back the memory.
  cudaMemcpy(A.data(), d_A, 3*ntpts*sizeof(double), cudaMemcpyDeviceToHost);

  // Free device memory.
  cudaFree(d_tpts);
  cudaFree(d_tri);
  cudaFree(d_A);
}

void launch_DemagnetizingFieldKernel(
    multi_array<double, 2> const &tpts,
    multi_array<double, 2> const &vcl,
    multi_array<int,    2> const &til,
    multi_array<double, 2> const &m, 
    double                       eps,
    double                       mu0,
    multi_array<double, 2>       &H)
{
  double x0 = 0.0, y0 = 0.0, z0 = 0.0;
  double x1 = 0.0, y1 = 0.0, z1 = 0.0;
  double x2 = 0.0, y2 = 0.0, z2 = 0.0;

  double mx = m[0][0], my = m[0][1], mz = m[0][2];

  unsigned int ntpts = tpts.shape()[0];
  unsigned int ntri = til.shape()[0];

  double *d_tpts;
  double *d_tri;
  double *d_H;

  int nblocks = 0;  

  // Resize the output array.
  cout << "Resizing vector potentials array" << endl;
  H.resize(extents[ntpts][3]);
  cout << "... OK!" << endl;

  // Construct the triangle array.
  cout << "Setting up triangles array." << endl;
  multi_array<double, 2> tri(extents[ntri][9]);
  for (int i = 0; i < ntri; ++i) {
    x0 = vcl[til[i][0]][0]; y0 = vcl[til[i][0]][1]; z0 = vcl[til[i][0]][2];
    x1 = vcl[til[i][1]][0]; y1 = vcl[til[i][1]][1]; z1 = vcl[til[i][1]][2];
    x2 = vcl[til[i][2]][0]; y2 = vcl[til[i][2]][1]; z2 = vcl[til[i][2]][2];
    tri[i][0] = x0; tri[i][1] = y0; tri[i][2] = z0;
    tri[i][3] = x1; tri[i][4] = y1; tri[i][5] = z1;
    tri[i][6] = x2; tri[i][7] = y2; tri[i][8] = z2;
  }
  cout << "... OK!" << endl;

  // Create storage on the device.
  cudaMalloc(&d_tpts, 3*ntpts*sizeof(double));
  cudaMalloc(&d_tri,  9*ntri*sizeof(double));
  cudaMalloc(&d_H,    3*ntpts*sizeof(double));

  // Copy memory over to the device.
  cudaMemcpy(d_tpts, tpts.data(), 3*ntpts*sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy(d_tri,  tri.data(),  9*ntri*sizeof(double),  cudaMemcpyHostToDevice);
  cudaMemcpy(d_H,    H.data(),    3*ntpts*sizeof(double), cudaMemcpyHostToDevice);

  // Execute the kernel.
  nblocks = ceil((double)ntpts / (double)THREADS_PER_BLOCK);
  DemagnetizingFieldKernel<<<nblocks, THREADS_PER_BLOCK>>>(
      d_tpts, ntpts, d_tri,  ntri, mx, my, mz, eps, mu0, d_H);
  cudaDeviceSynchronize();

  // Copy back the memory.
  cudaMemcpy(H.data(), d_H, 3*ntpts*sizeof(double), cudaMemcpyDeviceToHost);

  // Free device memory.
  cudaFree(d_tpts);
  cudaFree(d_tri);
  cudaFree(d_H);
}
