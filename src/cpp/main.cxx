#include <cmath>
#include <iostream>
#include <vector>
#include <fstream>
#include <cstdio>

#include <unistd.h>

#include <boost/multi_array.hpp>
#include <boost/format.hpp>
#include <boost/program_options.hpp>

#include "fabbri.h"

using namespace std;
using namespace boost;

namespace po = boost::program_options;

int main(int argc, char **argv) {

  // Input file of sample points (for now its just a patran file).
  string inFile;

  // The output vector potential file.
  string outFile;

  // The region within which to sample the vector potential.
  double xmin = -0.06, xmax = 0.06;
  double ymin = -0.06, ymax = 0.06;
  double zmin = -0.06, zmax = 0.06; 

  // The magnetisation vector.
  double mx = 0.0, my = 0.0, mz = 1.0;

  // Handle the input.
  po::options_description desc("Calculate vector potential for a given geometry");
  desc.add_options()
    ("help,h", "produce help message")
    ("in-file,i",       po::value<string>(), "the input file (patran sample points)")
    ("out-file,o",      po::value<string>(), "the output file name")
    ("xmin",            po::value<double>(), "box x coordinate minimum")
    ("xmax",            po::value<double>(), "box x coordinate maximum")
    ("ymin",            po::value<double>(), "box y coordinate minimum")
    ("ymax",            po::value<double>(), "box y coordinate maximum")
    ("zmin",            po::value<double>(), "box z coordinate minimum")
    ("zmax",            po::value<double>(), "box z coordinate maximum")
    ("mx",              po::value<double>(), "magnetisation x component")
    ("my",              po::value<double>(), "magnetisation y component")
    ("mz",              po::value<double>(), "magnetisation z component")
  ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    cout << desc << endl;
    return 0;
  }

  if (vm.count("sample-points")) {
    N = vm["sample-points"].as<int>();
  } else {
    cout << "Number of sample points not set." << endl;
    cout << desc << endl;
    exit(1);
  }

  if (vm.count("out-file")) {
    outFile = vm["out-file"].as<string>();
  } else {
    cout << "Output file not set." << endl;
    cout << desc << endl;
    exit(1);
  }

  if (vm.count("xmin")) {
    xmin = vm["xmin"].as<double>();
  }
  if (vm.count("xmax")) {
    xmax = vm["xmax"].as<double>();
  }

  if (vm.count("ymin")) {
    ymin = vm["ymin"].as<double>();
  }
  if (vm.count("ymax")) {
    ymax = vm["ymax"].as<double>();
  }
    
  if (vm.count("zmin")) {
    zmin = vm["zmin"].as<double>();
  }
  if (vm.count("zmax")) {
    zmax = vm["zmax"].as<double>();
  }

  if (vm.count("mx")) {
    mx = vm["mx"].as<double>();
  }
  if (vm.count("my")) {
    my = vm["my"].as<double>();
  }
  if (vm.count("mz")) {
    mz = vm["mz"].as<double>();
  }

  cout << format("No. of sample pts: %d (%d)") % N % (N*N*N) << endl;
  cout << format("      Box x range: %20.15f    to %20.15f") % xmin % xmax << endl;
  cout << format("      Box y range: %20.15f    to %20.15f") % ymin % ymax << endl;
  cout << format("      Box z range: %20.15f    to %20.15f") % zmin % zmax << endl;
  cout << format("  Magnetization x: %20.15f") % mx << endl;
  cout << format("  Magnetization y: %20.15f") % my << endl;
  cout << format("  Magnetization z: %20.15f") % mz << endl;
    
  // Construct an NX*NY*NZ by 3 array of points where the field will be sampled.
  int NX = N, NY = N, NZ = N;

  double dx = (xmax-xmin)/(double)(NX-1);
  double dy = (ymax-ymin)/(double)(NY-1);
  double dz = (zmax-zmin)/(double)(NZ-1);

  double x = 0.0, y = 0.0, z = 0.0;

  multi_array<double, 2> tpts(extents[NX*NY*NZ][3]);
  int l = 0;
  for (int i = 0; i < NZ; ++i) {
    for (int j = 0; j < NY; ++j) {
      for (int k = 0; k < NZ; ++k) {
        x = xmin + dx*(double)k;
        y = ymin + dy*(double)j;
        z = zmin + dz*(double)i;

        tpts[l][0] = x;
        tpts[l][1] = y;
        tpts[l][2] = z;

        ++l;
      }
    }
  }  
 
  // These are the vertices of the cuboctahedron.
  double dvcl[24][3] = {
    { 0.0527245,  0.0225962,  0.0000000}, 
    { 0.0527245,  0.0000000, -0.0225962}, 
    { 0.0527245, -0.0225962,  0.0000000}, 
    { 0.0527245,  0.0000000,  0.0225962}, 
    {-0.0527245,  0.0225962,  0.0000000}, 
    {-0.0527245,  0.0000000, -0.0225962}, 
    {-0.0527245, -0.0225962,  0.0000000}, 
    {-0.0527245,  0.0000000,  0.0225962}, 
    { 0.0000000,  0.0527245,  0.0225962}, 
    {-0.0225962,  0.0527245,  0.0000000}, 
    { 0.0000000,  0.0527245, -0.0225962}, 
    { 0.0225962,  0.0527245,  0.0000000}, 
    { 0.0000000, -0.0527245,  0.0225962}, 
    {-0.0225962, -0.0527245,  0.0000000}, 
    { 0.0000000, -0.0527245, -0.0225962}, 
    { 0.0225962, -0.0527245,  0.0000000}, 
    { 0.0000000,  0.0225962,  0.0527245}, 
    { 0.0225962,  0.0000000,  0.0527245}, 
    { 0.0000000, -0.0225962,  0.0527245}, 
    {-0.0225962,  0.0000000,  0.0527245}, 
    { 0.0000000,  0.0225962, -0.0527245}, 
    { 0.0225962,  0.0000000, -0.0527245}, 
    { 0.0000000, -0.0225962, -0.0527245}, 
    {-0.0225962,  0.0000000, -0.0527245} };

  // This is the triangle connectivity of the cuboctahedron.
  int dtil[44][3] = {
    { 0,  2,  1}, { 0,  3,  2}, { 4,  5,  6}, { 6,  7,  4}, {11,  9,  8}, 
    {11, 10,  9}, {14, 15, 12}, {14, 12, 13}, {18, 17, 16}, {18, 16, 19}, 
    {20, 21, 22}, {20, 22, 23}, {19,  7,  6}, {18, 13, 12}, {19,  6, 13}, 
    {18, 19, 13}, { 8,  9,  4}, {16,  7, 19}, { 8,  4,  7}, { 8,  7, 16}, 
    {10, 20, 23}, { 9,  5,  4}, {10, 23,  5}, {10,  5,  9}, { 5, 23, 22}, 
    { 5, 22, 14}, { 5, 14,  6}, { 6, 14, 13}, {20, 10, 11}, {20, 11, 21}, 
    {21, 11,  0}, {21,  0,  1}, { 1,  2, 15}, { 1, 15, 14}, { 1, 14, 21}, 
    {21, 14, 22}, { 2,  3, 17}, { 2, 17, 18}, { 2, 18, 15}, {15, 18, 12}, 
    { 0, 11,  8}, { 0,  8, 16}, { 0, 16,  3}, { 3, 16, 17}
  };
  
  // Create vertex coordinate list (vcl).
  boost::multi_array<double, 2> vcl(boost::extents[24][3]);
  for (int i = 0; i < 24; ++i)
    for (int j = 0; j < 3; ++j) 
      vcl[i][j] = dvcl[i][j];

  // Create tet index list (til)
  boost::multi_array<int, 2>    til(boost::extents[44][3]);
  for (int i = 0; i < 44; ++i)
    for (int j = 0; j < 3; ++j)
      til[i][j] = dtil[i][j];

  // Magnetizations (we only put one magnetization in for now).
  boost::multi_array<double, 2> m(boost::extents[1][3]);
  m[0][0] = mx; m[0][1] = my; m[0][2] = mz;

  // Fabbri epsilon.
  double eps = 1E-8;

  // Permeability of free space.
  double mu0 = 4.0 * M_PI * 1E-7;

  // Output (vector potential, magnetic flux density, demagneizing field) will 
  // be stored here.
  boost::multi_array<double, 2> output;

  cout << "Executing kernel" << endl;
  launch_DemagnetizingFieldKernel(tpts, vcl, til, m, eps, mu0, A);
  cout << "ALL DONE!!" << endl;

  fstream fout;
  fout.open(outFile.c_str(), fstream::out);
  for (int i = 0; i < A.shape()[0]; ++i) {
    fout << format("%20.15f %20.15f %20.15f %20.15f %20.15f %20.15f") % tpts[i][0] % tpts[i][1] % tpts[i][2] % A[i][0] % A[i][1] % A[i][2] << endl;
  }
  fout.close();

  return 0;
}
