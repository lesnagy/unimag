/**
 * @file:   fabbri.h
 * @author: L. Nagy
 */

#ifndef FABBRI_H
#define FABBRI_H

#include <vector>
#include <string>
#include <fstream>
#include <cstdio>
#include <ctime>

#include <boost/multi_array.hpp>

void launch_VectorPotentialKernel(
    boost::multi_array<double, 2> const &tpts,
    boost::multi_array<double, 2> const &vcl,
    boost::multi_array<int,    2> const &til,
    boost::multi_array<double, 2> const &m, 
    double                              eps,
    double                              mu0,
    boost::multi_array<double, 2>       &A);

void launch_MagneticFluxDensityKernel(
    boost::multi_array<double, 2> const &tpts,
    boost::multi_array<double, 2> const &vcl,
    boost::multi_array<int,    2> const &til,
    boost::multi_array<double, 2> const &m, 
    double                              eps,
    double                              mu0,
    boost::multi_array<double, 2>       &A);

void launch_DemagnetizingFieldKernel(
    boost::multi_array<double, 2> const &tpts,
    boost::multi_array<double, 2> const &vcl,
    boost::multi_array<int,    2> const &til,
    boost::multi_array<double, 2> const &m, 
    double                              eps,
    double                              mu0,
    boost::multi_array<double, 2>       &H);

#endif
